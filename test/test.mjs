import * as arg from "./node_modules/@jcain/asserts-cb/arg.mjs";

try {
	arg.assertNull(null);
	console.log("PASS");
}
catch (ex) {
	console.error("FAIL");
	console.error(ex);
}

try {
	arg.assertString(null);
	console.error("FAIL");
}
catch (ex) {
	console.log("PASS");
	console.log(ex);
}

try {
	arg.assertArrayNonEmpty([]);
	console.error("FAIL");
}
catch (ex) {
	console.log("PASS");
	console.log(ex);
}