# @jcain\asserts-cb\arg.mjs


## Functions

_Stability: **alpha**, Since: **0.0**_

Functions for checking the arguments passed to a method or function.


#### isUndefined()

```javascript
export function assertUndefined(value : Any, name : String = '') : undefined
```


#### isNotUndefined()

```javascript
export function assertNotUndefined(value : Any, name : String = '') : Any
```


#### isNull()

```javascript
export function assertNull(value : Any, name : String = '') : null
```


#### isNullish()

```javascript
export function assertNullish(value : Any, name : String = '') : Nullish
```


#### isNotNullish()

```javascript
export function assertNotNullish(value : Any, name : String = '') : Any
```


#### isBoolean()

```javascript
export function assertBoolean(value : Any, name : String = '') : Boolean
```


#### isBooleanOrNullish()

```javascript
export function assertBooleanOrNullish(value : Any, name : String = '') : Boolean|Nullish
```


#### isNumber()

```javascript
export function assertNumber(value : Any, name : String = '') : Number
```


#### isNumberOrNullish()

```javascript
export function assertNumberOrNullish(value : Any, name : String = '') : Number|Nullish
```


#### isInteger()

```javascript
export function assertInteger(value : Any, name : String = '') : Integer
```


#### isIntegerOrNullish()

```javascript
export function assertIntegerOrNullish(value : Any, name : String = '') : Integer|Nullish
```


#### isString()

```javascript
export function assertString(value : Any, name : String = '') : String
```


#### isStringOrNullish()

```javascript
export function assertStringOrNullish(value : Any, name : String = '') : String|Nullish
```


#### isArray()

```javascript
export function assertArray(value : Any, name : String = '') : Array
```


#### isArrayOrNullish()

```javascript
export function assertArrayOrNullish(value : Any, name : String = '') : Array|Nullish
```


#### isArrayNonEmpty()

```javascript
export function assertArrayNonEmpty(value : Any, name : String = '') : Array
```


#### isObject()

```javascript
export function assertObject(value : Any, name : String = '') : Object
```


#### isObjectOrNullish()

```javascript
export function assertObjectOrNullish(value : Any, name : String = '') : Object|Nullish
```


#### isFunction()

```javascript
export function assertFunction(value : Any, name : String = '') : Function
```


#### isFunctionOrNullish()

```javascript
export function assertFunctionOrNullish(value : Any, name : String = '') : Function|Nullish
```


#### isSymbol()

```javascript
export function assertSymbol(value : Any, name : String = '') : Symbol
```


#### isSymbolOrNullish()

```javascript
export function assertSymbolOrNullish(value : Any, name : String = '') : Symbol|Nullish
```


#### isInstance()

```javascript
export function assertInstance(type : Object, value : Any, name : String = '') : Object
```


#### isInstanceOrNullish()

```javascript
export function assertInstanceOrNullish(type : Object, value : Any, name : String = '') : Object|Nullish
```