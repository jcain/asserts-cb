# @jcain\asserts-cb\typeOf.mjs


## Functions


#### typeOf()

_Stability: **alpha**, Since: **0.0**_

Returns a string indicating the type of a value.

```javascript
export default function typeOf(value : any) : string
```

Similar to the native `typeof` operator, except this function includes `"null"` and `"array"`.


##### Return value

A `string` that is one of the following:

* `"undefined"`
* `"null"`
* `"boolean"`
* `"number"`
* `"string"`
* `"array"`
* `"object"`
* `"function"`
* `"symbol"`