import * as throwables from './throwables.mjs';
import typeOf from './typeOf.mjs';


export function isUndefined(value, name) {
	if (value !== undefined) {
		throw new TypeError(`${name ? name + ' : ' : ''}Expected undefined, got ${typeOf(value)}`);
	}
	return value;
}


export function isNotUndefined(value, name) {
	if (value === undefined) {
		throw new TypeError(`${name ? name + ' : ' : ''}Expected anything but undefined, got undefined`);
	}
	return value;
}


export function isNull(value, name) {
	if (value !== null) {
		throw new TypeError(`${name ? name + ' : ' : ''}Expected null, got ${typeOf(value)}`);
	}
	return value;
}


export function isNullish(value, name) {
	if (value != null) {
		throw new TypeError(`${name ? name + ' : ' : ''}Expected null or undefined, got ${typeOf(value)}`);
	}
	return value;
}


export function isNotNullish(value, name) {
	if (value == null) {
		throw new TypeError(`${name ? name + ' : ' : ''}Expected anything but null or undefined, got ${typeOf(value)}`);
	}
	return value;
}


export function isBoolean(value, name) {
	if (typeof value !== 'boolean') {
		throw throwables.newTypeError(name, 'boolean', value);
	}
	return value;
}


export function isBooleanOrNullish(value, name) {
	if (value != null && typeof value !== 'boolean') {
		throw throwables.newTypeError(name, 'boolean, null, or undefined', value);
	}
	return value;
}


export function isNumber(value, name) {
	if (typeof value !== 'number') {
		throw throwables.newTypeError(name, 'number', value);
	}
	return value;
}


export function isNumberOrNullish(value, name) {
	if (value != null && typeof value !== 'number') {
		throw throwables.newTypeError(name, 'number, null, or undefined', value);
	}
	return value;
}


export function isInteger(value, name) {
	if (typeof value !== 'number' && value !== Math.trunc(value)) {
		throw throwables.newTypeError(name, 'integer', value);
	}
	return value;
}


export function isIntegerOrNullish(value, name) {
	if (value != null && typeof value !== 'number') {
		throw throwables.newTypeError(name, 'integer, null, or undefined', value);
	}
	return value;
}


export function isString(value, name) {
	if (typeof value !== 'string') {
		throw throwables.newTypeError(name, 'string', value);
	}
	return value;
}


export function isStringOrNullish(value, name) {
	if (value != null && typeof value !== 'string') {
		throw throwables.newTypeError(name, 'string, null, or undefined', value);
	}
	return value;
}


export function isArray(value, name) {
	if (typeof value !== 'object' && !Array.isArray(value)) {
		throw throwables.newTypeError(name, 'array', value);
	}
	return value;
}


export function isArrayOrNullish(value, name) {
	if (value != null && typeof value !== 'object' && !Array.isArray(value)) {
		throw throwables.newTypeError(name, 'array, null, or undefined', value);
	}
	return value;
}


export function isArrayNonEmpty(value, name) {
	isArray(value, name);
	if (!value.length) {
		throw throwables.newValueError(name, 'length > 0', 'length == 0');
	}
	return value;
}


export function isObject(value, name) {
	if (value == null || typeof value !== 'object') {
		throw throwables.newTypeError(name, 'object', value);
	}
	return value;
}


export function isObjectOrNullish(value, name) {
	if (value != null && typeof value !== 'object') {
		throw throwables.newTypeError(name, 'object, null, or undefined', value);
	}
	return value;
}


export function isFunction(value, name) {
	if (typeof value !== 'function') {
		throw throwables.newTypeError(name, 'function', value);
	}
	return value;
}


export function isFunctionOrNullish(value, name) {
	if (value != null && typeof value !== 'function') {
		throw throwables.newTypeError(name, 'function, null, or undefined', value);
	}
	return value;
}


export function isSymbol(value, name) {
	if (typeof value !== 'symbol') {
		throw throwables.newTypeError(name, 'symbol', value);
	}
	return value;
}


export function isSymbolOrNullish(value, name) {
	if (value != null && typeof value !== 'symbol') {
		throw throwables.newTypeError(name, 'symbol, null, or undefined', value);
	}
	return value;
}


export function isInstance(type, value, name) {
	if (!(value instanceof type)) {
		throw throwables.newTypeError(name, 'instance of ' + (type.name || type), (value != null && value.constructor && value.constructor.name || typeOf(value)));
	}
	return value;
}


export function isInstanceOrNullish(type, value, name) {
	if (value != null && !(value instanceof type)) {
		throw throwables.newTypeError(name, 'instance of ' + (type.name || type) + ', null, or undefined', (value != null && value.constructor && value.constructor.name || typeOf(value)));
	}
	return value;
}