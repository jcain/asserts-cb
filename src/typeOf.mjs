export default function typeOf(value) {
	if (value === null) {
		return 'null';
	}

	let type = typeof value;
	if (type === 'object' && Array.isArray(value)) {
		return 'array';
	}

	return type;
}