import typeOf from "./typeOf.mjs";
import ValueError from "./ValueError.mjs";


export function newTypeError(name, expected, got) {
	return new TypeError(`${name ? name + " : " : ""}Expected ${expected}, got ${typeOf(got)}`);
}


export function newValueError(name, expected, got) {
	return new ValueError(`${name ? name + " : " : ""}Expected ${expected}, got ${got}`);
}