# @jcain\asserts-cb\throwables.mjs


## Functions


#### newTypeError()

_Stability: **alpha**, Since: **0.0**_

Returns a new [`TypeError`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/TypeError) instance with a formatted message.

```javascript
export function newTypeError(name : string, expected : string, got : any) : TypeError
```


#### newValueError()

_Stability: **alpha**, Since: **0.0**_

Returns a new [`TypeValue`](TypeValue.api.md) instance with a formatted message.

```javascript
export function newValueError(name : string, expected : string, got : any) : ValueError
```