export default class ValueError extends TypeError {
	constructor(message) {
		super(message);
		this.name = this.constructor.name;
	}
}